FROM python:3.9-slim-buster

WORKDIR /test-app

RUN apt-get update


COPY requires .

RUN pip3 install -r requires

COPY . .

EXPOSE 5000

ENV FLASK_APP=flaskapp

CMD ["flask", "run", "--host", "0.0.0.0"]
